# peerspire

Peerspire is a streaming torrent client for Node.js

```
npm install -g peerspire
```


## Usage

Peerspire can be used with a magnet link or a torrent file.
To stream a video with its magnet link use the following command.

```
peerspire "magnet:?xt=urn:btih:ef330b39f4801d25b4245212e75a38634bfc856e" --vlc
```

Remember to put `"` around your magnet link since they usually contain `&`.
`peerspire` will print a terminal interface. The first line contains an address to a http server. The `--vlc` flag ensures vlc is opened when the torrent is ready to stream.

![peerspire](https://gitlab.com/theinspire.eu/peerflix/blob/master/screenshot.png)

To stream music with a torrent file use the following command.

```
peerspire "http://some-torrent/music.torrent" -a --vlc
```

The `-a` flag ensures that all files in the music repository are played with vlc.
Otherwise if the torrent contains multiple files, `peerspire` will choose the biggest one.
To get a full list of available options run peerflix with the help flag.

```
peerspire --help
```

Examples of usage of could be

```
peerspire magnet-link --list # Select from a list of files to download
peerspire magnet-link --vlc -- --fullscreen # will pass --fullscreen to vlc
peerspire magnet-link --mplayer --subtitles subtitle-file.srt # play in mplayer with subtitles
peerspire magnet-link --connection 200 # set max connection to 200
```


## Programmatic usage

You can use peerspire to build your streaming apps. 

## Chromebook users

Chromebooks are set to refuse all incoming connections by default - to change this:  

```
sudo iptables -P INPUT ACCEPT
```

## License

MIT
